import React from "react";
import Profile from "../HomePage/Profile";

const Skills = (props) => {
  return (
    <Profile history={props.history}>
      <div className="position-relative d-flex">
        <div className=""></div>
        <div className="about">
          <div className="html-content"></div>
          <div className="body-content"></div>

          <h1>My Skills</h1>

          <div className="text">
            <button>Html</button>
            <button>Css</button>
            <button>Sass</button>
            <button>Bootstrap</button>
            <button>Material UI</button>
            <button>Ant Design</button>
            <button>JavaScript</button>
            <button>TypeScript</button>
            <button>React.js</button>
            <button>Next.js</button>
            <button>react-i18next</button>
            <button>Redux</button>
          </div>

          <div className="html-content-b"></div>
          <div className="body-content-b"></div>
        </div>
      </div>
    </Profile>
  );
};

export default Skills;
