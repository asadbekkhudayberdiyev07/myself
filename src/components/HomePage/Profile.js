import React from "react";
import "../../styles/main.scss";
import { Link } from "react-router-dom";

const Profile = (props) => {
  const goToHome = () => {
    props.history.push("/");
  };
  const waitingMenu = () => {
    alert("waiting page");
  };
  return (
    <div className="profile">
      <div className="profile-left">
        <div className="profile-top" onClick={goToHome}>
          <div className="my-image">
            <img src="/images/iam.jpg" alt="my image" />
          </div>
          <h3>Asadbek</h3>
          <p>Frontend Developer</p>
        </div>

        <div className="menus-all">
          <div className="menus">
            <Link to="/about">
              <p
                className={`${
                  props.history.location.pathname === "/about" ? "active" : ""
                }`}
              >
                About
              </p>
            </Link>
          </div>
          <div className="menus">
            {/* <Link to="/my-skills"> */}
            <p
              className={`${
                props.history.location.pathname === "/my-skills" ? "active" : ""
              }`}
              onClick={waitingMenu}
            >
              My Skills
            </p>
            {/* </Link> */}
          </div>
          <div className="menus">
            <p onClick={waitingMenu}>Work</p>
          </div>
          <div className="menus">
            <p onClick={waitingMenu}>Blog</p>
          </div>
          {/* <div className="menus">
            <p onClick={waitingMenu}>Contact</p>
          </div> */}
        </div>
        <div className="media-menu text">
          I’m a Front-End Developer located in Tashkent. I have a serious
          passion for UI effects, animations and creating intuitive, dynamic
          user experiences.
        </div>

        <div className="contact-me d-flex align-items-center justify-content-center">
          <div className="d-flex align-items-center">
            <a href="https://t.me/asad_book" target="_blank">
              <span className="icon icon-telegram"></span>
            </a>
            <a
              href="https://www.linkedin.com/in/asadbek-xudayberdiyev-610978247/"
              target="_blank"
            >
              {" "}
              <span className="icon icon-linkedin mx-3"></span>
            </a>
            <a href="https://www.instagram.com/asad_boook/" target="_blank">
              <span className="icon icon-instagram"></span>
            </a>
            <a href="https://www.instagram.com/asad_boook/" target="_blank">
              <span className="icon icon-facebook"></span>
            </a>
          </div>
        </div>
        <button className="btn btn-success">
          <a href="tel:+998934722477">
            <p className="mb-0">Contact me!</p>
            <div className="button-position"></div>
          </a>
        </button>
      </div>

      <div className="profile-right">{props.children}</div>
    </div>
  );
};

export default Profile;
