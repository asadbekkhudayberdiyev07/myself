import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./components/HomePage/Home";
import About from "./components/About/About";
import Skills from "./components/Skills/Skills";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/about" exact component={About} />
        <Route path="/my-skills" exact component={Skills} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
